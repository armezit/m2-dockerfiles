# Tags

* `armezit/m2-nginx:1.13` [(Dockerfile)](https://github.com/armezit/m2-dockerfiles/blob/master/nginx/1.13/Dockerfile)
* `armezit/m2-nginx:mainline-alpine` [(Dockerfile)](https://github.com/armezit/m2-dockerfiles/blob/master/nginx/mainline-alpine/Dockerfile)

# Description

Nginx images based on `debian:stretch-slim` and `alpine`.