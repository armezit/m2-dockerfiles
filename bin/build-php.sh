#! /usr/bin/env bash

# Exit in case of error
set -e

PROJECT_DIR=$(dirname $(dirname $(readlink -f $0)))

function help_menu () {
cat << EOF
Usage: ${0} [OPTIONS]

OPTIONS:
   --version    PHP version [required]
   --target     Docker build target (dev|prod); default is "dev"
   --tag        Docker image tag; Pass multiple tags separated by comma: --tag=tag1,tag2
   -h|--help    Show this message
EOF
}

#---------------------------
# Parse arguments
#---------------------------
for i in "$@"
do
case ${i} in
    --version=*)
    PHP_VERSION="${i#*=}"
    shift
    ;;
    --target=*)
    TARGET="${i#*=}"
    shift
    ;;
    --tag=*)
    IFS=',' read -r -a tags_array <<< "${i#*=}"
    for tag in "${tags_array[@]}"; do
        TAGS="-t ${tag} ${TAGS}"
    done
    shift
    ;;
    -h|--help)
    help_menu
    shift
    exit 0
    ;;
    *)
    # unknown option
    echo "${i} is not a valid flag, try running: ${0} --help"
    exit 1
    ;;
esac
done

if [[ -z ${PHP_VERSION} ]]; then
    echo -e "error: --version argument missing\n"
    help_menu
    exit 1
fi

#---------------------------
# Set default values
#---------------------------
TARGET="${TARGET:-dev}"
if [[ ${TARGET} == "prod" ]]; then
  DEFAULT_TAG="-t armezit/m2-php:${PHP_VERSION}"
else
  DEFAULT_TAG="-t armezit/m2-php:${PHP_VERSION}-${TARGET}"
fi
TAGS=${TAGS:-${DEFAULT_TAG}}

echo -e "Running php docker image build for target=[${TARGET}] with tags=[${TAGS}]..."

#---------------------------
# Build php image
#---------------------------
DOCKER_BUILDKIT=1 \
docker build \
    --progress=plain \
    --target=${TARGET} \
    ${TAGS} \
    -f "${PROJECT_DIR}/php/${PHP_VERSION}/Dockerfile" \
    "${PROJECT_DIR}/php/${PHP_VERSION}"
