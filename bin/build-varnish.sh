#! /usr/bin/env bash

# Exit in case of error
set -e

PROJECT_DIR=$(dirname $(dirname $(readlink -f $0)))

ENV_FILE="${PROJECT_DIR}/.env"
if [[ -f "${ENV_FILE}" ]]; then
  source "${ENV_FILE}"
fi

function help_menu () {
cat << EOF
Usage: ${0} [OPTIONS]

OPTIONS:
   --version    Varnish version [required]
   --tag        Docker image tag; Pass multiple tags separated by comma: --tag=tag1,tag2
   -h|--help    Show this message
EOF
}

#---------------------------
# Parse arguments
#---------------------------
for i in "$@"
do
case ${i} in
    --version=*)
    VARNISH_VERSION="${i#*=}"
    shift
    ;;
    --tag=*)
    IFS=',' read -r -a tags_array <<< "${i#*=}"
    for tag in "${tags_array[@]}"; do
        TAGS="-t ${tag} ${TAGS}"
    done
    shift
    ;;
    -h|--help)
    help_menu
    shift
    exit 0
    ;;
    *)
    # unknown option
    echo "${i} is not a valid flag, try running: ${0} --help"
    exit 1
    ;;
esac
done

if [[ -z ${VARNISH_VERSION} ]]; then
    echo -e "error: --version argument missing\n"
    help_menu
    exit 1
fi

#---------------------------
# Set default values
#---------------------------
DEFAULT_TAG="-t armezit/m2-varnish:${VARNISH_VERSION}"
TAGS=${TAGS:-${DEFAULT_TAG}}

echo -e "Running varnish docker image build with tags=[${TAGS}]..."

#---------------------------
# BUILD image
#---------------------------
DOCKER_BUILDKIT=1 \
docker build \
    --ssh default \
    --progress=plain \
    ${TAGS} \
    -f "${PROJECT_DIR}/varnish/${VARNISH_VERSION}/Dockerfile" \
    "${PROJECT_DIR}/varnish/${VARNISH_VERSION}"
