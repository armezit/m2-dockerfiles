#! /usr/bin/env bash

# Exit in case of error
set -e

PROJECT_DIR=$(dirname $(dirname $(readlink -f $0)))

function help_menu () {
cat << EOF
Usage: ${0} [OPTIONS]

OPTIONS:
   --version    Nginx version; default is "1.15-alpine"
   --target     Docker build target (dev|prod); default is "dev"
   --tag        Docker image tag; Pass multiple tags separated by comma: --tag=tag1,tag2
   -h|--help    Show this message
EOF
}

#---------------------------
# Parse arguments
#---------------------------
for i in "$@"
do
case ${i} in
    --version=*)
    NGINX_VERSION="${i#*=}"
    shift
    ;;
    --target=*)
    TARGET="${i#*=}"
    shift
    ;;
    --tag=*)
    IFS=',' read -r -a tags_array <<< "${i#*=}"
    for tag in "${tags_array[@]}"; do
        TAGS="-t ${tag} ${TAGS}"
    done
    shift
    ;;
    -h|--help)
    help_menu
    shift
    exit 0
    ;;
    *)
    # unknown option
    echo "${i} is not a valid flag, try running: ${0} --help"
    exit 1
    ;;
esac
done

#---------------------------
# Set default values
#---------------------------
NGINX_VERSION="${NGINX_VERSION:-1.15-alpine}"
TARGET="${TARGET:-dev}"
if [[ ${TARGET} == "prod" ]]; then
  DEFAULT_TAG="-t armezit/m2-nginx:${NGINX_VERSION}"
else
  DEFAULT_TAG="-t armezit/m2-nginx:${NGINX_VERSION}-${TARGET}"
fi
TAGS=${TAGS:-${DEFAULT_TAG}}


echo -e "Running nginx docker image build for target=[${TARGET}] with tags=[${TAGS}] ..."

#---------------------------
# BUILD nginx image
#---------------------------
DOCKER_BUILDKIT=1 \
docker build \
    --progress=plain \
    --target=${TARGET} \
    ${TAGS} \
    -f "${PROJECT_DIR}/nginx/${NGINX_VERSION}/Dockerfile" \
    "${PROJECT_DIR}/nginx/${NGINX_VERSION}"
