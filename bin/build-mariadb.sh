#! /usr/bin/env bash

# Exit in case of error
set -e

PROJECT_DIR=$(dirname $(dirname $(readlink -f $0)))

ENV_FILE="${PROJECT_DIR}/.env"
if [[ -f "${ENV_FILE}" ]]; then
  source "${ENV_FILE}"
fi

function help_menu () {
cat << EOF
Usage: ${0} [OPTIONS]

OPTIONS:
   --version    version [required]
   --tag        Docker image tag; Pass multiple tags separated by comma: --tag=tag1,tag2
   -h|--help    Show this message
EOF
}

#---------------------------
# Parse arguments
#---------------------------
for i in "$@"
do
case ${i} in
    --version=*)
    VERSION="${i#*=}"
    shift
    ;;
    --tag=*)
    IFS=',' read -r -a tags_array <<< "${i#*=}"
    for tag in "${tags_array[@]}"; do
        TAGS="-t ${tag} ${TAGS}"
    done
    shift
    ;;
    -h|--help)
    help_menu
    shift
    exit 0
    ;;
    *)
    # unknown option
    echo "${i} is not a valid flag, try running: ${0} --help"
    exit 1
    ;;
esac
done

#---------------------------
# Set default values
#---------------------------
VERSION="${VERSION:-10.4}"
DEFAULT_TAG="-t armezit/m2-mariadb:latest"
TAGS=${TAGS:-${DEFAULT_TAG}}

echo -e "Running mariadb docker image build with tags=[${TAGS}]..."

#---------------------------
# BUILD image
#---------------------------
DOCKER_BUILDKIT=1 \
docker build \
    --ssh default \
    --progress=plain \
    ${TAGS} \
    -f "${PROJECT_DIR}/mariadb/${VERSION}/Dockerfile" \
    "${PROJECT_DIR}/mariadb/${VERSION}"
