# Tags

* `armezit/m2-php:7.0-fpm` [(Dockerfile)](https://github.com/armezit/m2-dockerfiles/blob/master/php/7.0-fpm/Dockerfile) (**Depricated**)
* `armezit/m2-php:7.1-fpm` [(Dockerfile)](https://github.com/armezit/m2-dockerfiles/blob/master/php/7.1-fpm/Dockerfile) (**Depricated**)
* `armezit/m2-php:7.2-cron-alpine` [(Dockerfile)](https://github.com/armezit/m2-dockerfiles/blob/master/php/7.2-cron-alpine/Dockerfile)
* `armezit/m2-php:7.2-fpm-alpine` [(Dockerfile)](https://github.com/armezit/m2-dockerfiles/blob/master/php/7.2-fpm-alpine/Dockerfile)

# Description

PHP-fpm image with composer and xdebug installed.