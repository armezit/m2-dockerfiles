# Description
Elasticsearch docker image with additional plugins for [ElasticSuite module](https://github.com/Smile-SA/elasticsuite)


```shell
cd /usr/share/elasticsearch
bin/elasticsearch-plugin install analysis-phonetic
bin/elasticsearch-plugin install analysis-icu
```

## Known Issues :warning:

If you get error of low `vm.max_map_count`, on your host machine (not in docker container), increase it's value:

```bash
sudo sysctl -w vm.max_map_count=262144
```

