# Tags

* `armezit/node-php:node8-php7.2` [(Dockerfile)](https://github.com/armezit/m2-dockerfiles/blob/master/node-php/node8-php7.2/Dockerfile)
* `armezit/node-php:node8-php7.1` [(Dockerfile)](https://github.com/armezit/m2-dockerfiles/blob/master/node-php/node8-php7.1/Dockerfile)
* `armezit/node-php:node8-php7.0` [(Dockerfile)](https://github.com/armezit/m2-dockerfiles/blob/master/node-php/node8-php7.0/Dockerfile)

# Description

Image based on alpine with nodejs and php-fpm built in.